/*
 * Project event-driven-mailbox
 * Description:
 * Author:
 * Date:
 */

#include "Adafruit_VL53L0X.h"

#define TOF_GPIO_PIN D2
#define TOF_SHDN_PIN D3
#define ALS_PIN D4

Adafruit_VL53L0X lox = Adafruit_VL53L0X();

static bool is_dark = false;
static uint16_t distance = 0;
static bool ready_for_sleep = true;

bool measure_tof() {
   VL53L0X_RangingMeasurementData_t measure;

  // Take a measurement
  lox.rangingTest(&measure, false); // pass in 'true' to get debug data printout!

  // If in range, compare and then if valid send to cloud
  if (measure.RangeStatus != 4) {  // phase failures have incorrect data
    distance = measure.RangeMilliMeter;
    Serial.print("Distance (mm): "); Serial.println(measure.RangeMilliMeter);
    Particle.publish("distance",String::format("%imm",measure.RangeMilliMeter), PRIVATE);

    return true;
  }

  return false;
}

void check_als(bool first) {
  bool new_is_dark = digitalRead(ALS_PIN);
  bool update = false;

  // Handle if this is the first
  if( first ) {
    first = false;
    update = true;
  }

  if( !new_is_dark && is_dark != new_is_dark ) {
    is_dark = new_is_dark;
    digitalWrite(D7,HIGH);
    update = true;
  } else if( new_is_dark && is_dark != new_is_dark ) {
    is_dark = new_is_dark;
    digitalWrite(D7,LOW);
    update = true;

    // TODO: Set thsi on timer
    // TODO: beter location
  }

  // Update to the cloud
  if( update ) {
    Particle.publish("dark", is_dark ? "true" : "false", PRIVATE);
  }

}

// setup() runs once, when the device is first turned on.
void setup() {
  // Put initialization like pinMode and begin functions here.

  Serial.begin();

  RGB.control(true);
  RGB.brightness(0);

  pinMode(ALS_PIN, INPUT);
  pinMode(TOF_GPIO_PIN, INPUT);
  pinMode(TOF_SHDN_PIN, OUTPUT);
  pinMode(D7, OUTPUT);

  // Get initial reading
  is_dark = digitalRead(ALS_PIN);

  // Turn on LED if !dark
  if( !is_dark ) {
    digitalWrite(D7,HIGH);
  }

  // Check ALS
  check_als(true);

  // Make sure the TOF sensor is on
  digitalWrite(TOF_SHDN_PIN,HIGH);

  // Init TOF sensor
  if (!lox.begin(false)) {
    Serial.println(F("Failed to boot VL53L0X"));
  }

}

// loop() runs over and over again, as quickly as it can execute.
void loop() {
  // Check ALS
  check_als(false);

  // Handle power
  if( is_dark ) {

    Serial.println("is dark..");

    // Measure tof when about to go off
    if ( measure_tof()  ) {
      ready_for_sleep = true;
    }

    delay(100);
  }

  // Process messages
  Particle.process();

  // If we're good to sleep
  if( ready_for_sleep ) {
    // Reset this var
    ready_for_sleep = false;

    // Shut down TOF
    digitalWrite(TOF_SHDN_PIN,HIGH);

    // Sleep
    System.sleep(ALS_PIN,FALLING);

    // Turn it back on
    digitalWrite(TOF_SHDN_PIN,HIGH);
  }

}